import * as axios from "axios";


const instance=axios.create({
    baseURL:"http://localhost:5000/api/products/category/"
})

export const productsAPI={

  getProducts(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter){
    return  instance.get(`${parentCategory}/allproducts?page=${currentPage}&limit=${limit}&sortBy=${selectedTypeOfSort}&color=${selectedColorFilter}&size=${selectedSizeFilter}`)
      .then((res) => {
        return res.data;
      })   
  },
  getProductsOnChangeCategory(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter,subCategory){
    return instance.get(`${parentCategory}/allproducts?page=${currentPage}&limit=${limit}&sortBy=${selectedTypeOfSort}&color=${selectedColorFilter}&size=${selectedSizeFilter}&subCategory=${subCategory}`)
    .then((res) => {
        return res.data;
      })
  },
  getProductsOnPriceRangeChanged(parentCategory,currentPage,limit,selectedTypeOfSort,
    selectedColorFilter,selectedSizeFilter,test,selectedCategory){
    return instance.get(`${parentCategory}/allproducts?page=${currentPage}&limit=${limit
      }&sortBy=${selectedTypeOfSort}&color=${
        selectedColorFilter
      }&size=${selectedSizeFilter}&priceRange=${String(
        test
      )}&category=${selectedCategory}`)
    .then((res) => {
        return res.data;
    })
  },
  getProductsGameConsoles(){
    return  instance.get("electronics/gaming-Game-consoles")
      .then((res) => {
        return res.data;
      })  
},

 getProductsOnSearchTextChanged(parentCategory,searchText){
    return  instance.get(`${parentCategory}/search/products/${searchText}`)
      .then((res) => {
        return res.data;
      })  
},
getNewCategoriesOnParentCategoryChanged(parentCategory){
  return instance.get(`${parentCategory
    .split(" ")
    .join("")}/sorting`)
  .then((res)=>{
    return res.data;
  })
},

getSettingsOnResultRefresh(parentCategory,currentPage,limit,selectedTypeOfSort){
  return instance.get(`${parentCategory
    .split(" ")
    .join("")}/allproducts?page=${currentPage}&limit=${
    limit
  }&sortBy=${selectedTypeOfSort}`)
  .then((res)=>{
    return res.data;
  })
},

getCurrentConsoleItem(currentConsoleItemId){
  return instance.get(`electronics/gaming-Game-consoles/search-By/${currentConsoleItemId}`)
  .then((res)=>{
    return res.data;
  })
},

getItemDetails(parentCategory,id){
  return instance.get(`${parentCategory}/search-By/${id}`)
  .then((res)=>{
    return res.data;
  })
},

}



