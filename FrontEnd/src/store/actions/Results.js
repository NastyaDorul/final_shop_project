import {productsAPI} from "../../api/api";

function handleErrors(err) {
  if (err.response) {
    console.log("Error with response", err.response.status);
  } else if (err.request) {
    console.log("Error with request");
  } else {
    console.log("Error", err.message);
  }
}

export const setSearchResultAc = (result) => ({
  type: "SET_SEARCH_RESULT",
  payload: result,
});
export const setResetResultAc = (result) => ({
  type: "RESET_RESULT",
  payload: result,
});
export const setCurrentPageAc = (currentPage) => ({
  type: "SET_CURRENT_PAGE",
  payload: currentPage,
});
export const setCurrentPageResult = (pageResult) => ({
  type: "CURRENT_PAGE_RESULT",
  payload: pageResult,
});
export const setPageLimitAc = (limit) => ({
  type: "SET_PAGE_LIMIT",
  payload: limit,
});
export const setTotalProductCountAc = (total) => ({
  type: "SET_TOTAL_RPODUCT_COUNT",
  payload: total,
});
export const setCurrentTypeOfSortAc = (selectedTypeOfSort) => ({
  type: "SET_CURRENT_TYPE_OF_SORT",
  payload: selectedTypeOfSort,
});
export const setColorFilterAc = (selectedColorFilter) => ({
  type: "SET_COLOR_FILTER",
  payload: selectedColorFilter,
});
export const setSizeFilterAc = (selectedSizeFilter) => ({
  type: "SET_SIZE_FILTER",
  payload: selectedSizeFilter,
});

export const setPriceRangeAc = (selectedRange) => ({
  type: "SET_PRICE_RANGE",
  payload: selectedRange,
});
export const setBrandFilterAc = (selectedBrand) => ({
  type: "SET_BRAND_FILTER",
  payload: selectedBrand,
});
export const setGSFilterAc = (selectedGS) => ({
  type: "SET_GAMING_SYSTEM_FILTER",
  payload: selectedGS,
});

export const setSubCategoryAc = (subCategory) => ({
  type: "SET_SUB_CATEGORY",
  payload: subCategory,
});
export const setParentCategoryAc = (parentCategory) => ({
  type: "SET_PARENT_CATEGORY",
  payload: parentCategory,
});
export const setMinAc = (categoryPriceMin) => ({
  type: "SET_MIN",
  payload: categoryPriceMin,
});
export const setMaxAc = (categoryPriceMax) => ({
  type: "SET_MAX",
  payload: categoryPriceMax,
});

export const setItemDetailsAc = (itemDetails) => ({
  type: "SET_ITEM",
  payload: itemDetails,
});
export const setSortingItemsAc = (sortingItems) => ({
  type: "SET_SORTING_ITEMS",
  payload: sortingItems,
});
export const setSortingColorAc = (sortingColorItems) => ({
  type: "SET_SORTING_COLOR",
  payload: sortingColorItems,
});
export const setSortingSizeAc = (sortingSizeItems) => ({
  type: "SET_SORTING_SIZE",
  payload: sortingSizeItems,
});
export const setSortingPriceAc = (sortingPriceItems) => ({
  type: "SET_SORTING_PRICE",
  payload: sortingPriceItems,
});
export const setSortingBrandAc = (sortingBrandItems) => ({
  type: "SET_SORTING_BRAND",
  payload: sortingBrandItems,
});
export const setSortingGSAc = (sortingGSItems) => ({
  type: "SET_SORTING_GS",
  payload: sortingGSItems,
});
//---------------------------------------------------------------------
export const getProductsThunkCreator=(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter)=>{
  return (dispatch)=>{
      productsAPI.getProducts(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter)
      .then((data) => {
       dispatch(setSearchResultAc(data.result));
      })
      .catch(handleErrors);
  }
}

export const getProductsOnPriceRangeChangedThunkCreator=(parentCategory,currentPage,limit,
  selectedTypeOfSort,selectedColorFilter,selectedSizeFilter,
  test,selectedCategory)=>{
  return (dispath)=>{
    productsAPI.getProductsOnPriceRangeChanged(parentCategory,currentPage,limit,
      selectedTypeOfSort,selectedColorFilter,selectedSizeFilter,
      test,selectedCategory)
      .then((data) => {
        dispath(setSearchResultAc(data.result));
        console.log(data.result);
      })
      .catch(handleErrors);
  }
}

export const getProductsOnChangeCategoryThunkCreator=(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter,subCategory)=>{
  return(dispatch)=>{
    productsAPI.getProductsOnChangeCategory(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter,subCategory)
    .then((data) => {
      dispatch(setSearchResultAc(data.result));
      console.log(data.result);
    })
    .catch(handleErrors);
  }
}

export const getNewCategoriesOnParentCategoryChangedThunkCreator=(parentCategory)=>{
  return(dispatch)=>{
    dispatch(setParentCategoryAc(parentCategory));
    productsAPI.getNewCategoriesOnParentCategoryChanged(parentCategory)
      .then((data) => {
        let categories = data.find((item) => item.name === "subcategory");
        if (categories) {
          dispatch(setSortingItemsAc(categories.sortingvalues));
          console.log(categories.sortingvalues);
        } else {
          dispatch(setSortingItemsAc(null));
          console.log("this category does'not contain subcategory filter");
        }

        let selectedbrand = data.find(
          (item) => item.name === "selectedbrand"
        );
        if (selectedbrand) {
          dispatch(setSortingBrandAc(selectedbrand.sortingvalues));
          console.log(selectedbrand.sortingvalues);
        } else {
          dispatch(setSortingBrandAc(null));
          console.log("this category does'not contain selectedbrand filter");
        }

        let selectedsize = data.find(
          (item) => item.name === "selectedsize"
        );
        if (selectedsize) {
          dispatch(setSortingSizeAc(selectedsize.sortingvalues));
          console.log(selectedsize.sortingvalues);
        } else {
          dispatch(setSortingSizeAc(null));
          console.log("this category does'not contain selectedsize filter");
        }
        let selectedcolor = data.find(
          (item) => item.name === "selectedcolor"
        );
        if (selectedcolor) {
          dispatch(setSortingColorAc(selectedcolor.sortingvalues));
        } else {
          dispatch(setSortingColorAc(null));
          console.log("this category does'not contain selectedcolor filter");
        }

        let selectedgs = data.find((item) => item.name === "selectedgs");
        if (selectedgs) {
          dispatch(setSortingGSAc(selectedgs.sortingvalues));
        } else {
          dispatch(setSortingGSAc(null));
          console.log("this category does'not contain sortingGSItems filter");
        }
      })

      .catch(handleErrors);
  }
}

export const getSettingsOnResultRefreshThunkCreator=(parentCategory,currentPage,limit,selectedTypeOfSort)=>{
  return(dispatch)=>{
    productsAPI.getSettingsOnResultRefresh(parentCategory,currentPage,limit,selectedTypeOfSort)
    .then((data) => {
      dispatch(setResetResultAc(data.result));
      dispatch(setTotalProductCountAc(data.total));
      dispatch(setPageLimitAc(limit));
      dispatch(setCurrentPageAc(currentPage));
    })
    .catch(handleErrors);
  }
}

export const getCurrentConsoleItemThunkCreator=(currentConsoleItemId)=>{
  return(dispatch)=>{
    productsAPI.getCurrentConsoleItem(currentConsoleItemId)
    .then((data) => {
      dispatch(setCurrentConsoleItemAc(data));
      console.log(data);
    })
    .catch(handleErrors);
  }
}

export const getItemDetailsThunkCreator =(parentCategory,id)=>{
  return(dispatch)=>{
    productsAPI.getItemDetails(parentCategory,id)
    .then((data) => {
      console.log(data);
      dispatch(setItemDetailsAc(data));
    })
    .catch(handleErrors);
  }
}

//---------------------------------------------------------------------
export const addToCartAc = (addedProductId) => ({
  type: "ADD_TO_CART",
  payload: addedProductId,
});

export const removeFromCartAc = (id) => ({
  type: "REMOVE_FROM_CART",
  payload: id,
});
export const removeAllFromCart = (id) => ({
  type: "REMOVE_ALL_FROM_CART",
  payload: id,
});
//---------------------------------------------------------------------

export const setItemIdTitlePriceImgAc = (currentItemDetails) => ({
  type: "SET_DETAILS_OF_ITEM",
  payload: currentItemDetails,
});

export const setCurrentConsoleItemIdAc = (currentConsoleItemId) => ({
  type: "SET_CURRENT_CONSOLE_ITEM_ID",
  payload: currentConsoleItemId,
});

export const setCurrentConsoleItemAc = (ConsoleItem) => ({
  type: "SET_CURRENT_CONSOLE_ITEM",
  payload: ConsoleItem,
});
