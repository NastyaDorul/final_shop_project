import {productsAPI} from "../../api/api";

function handleErrors(err) {
  if (err.response) {
    console.log("Error with response", err.response.status);
  } else if (err.request) {
    console.log("Error with request");
  } else {
    console.log("Error", err.message);
  }
}

export const setSearchQueryAc = (searchText) => ({
  type: "SET_SEARCH_QUERY",
  payload: searchText,
});
export const setSearchQueryResultAc = (searchResult) => ({
  type: "SET_SEARCH_QUERY_RESULT",
  payload: searchResult,
});


export const getProductsOnSearchTextChangedThunkCreator=(parentCategory,searchText)=>{
  return (dispatch)=>{
      productsAPI.getProductsOnSearchTextChanged(parentCategory,searchText)
      .then((data) => {
       dispatch(setSearchQueryResultAc(data));
       console.log(data);
      })
      .catch(handleErrors);
  }
}

