import {productsAPI} from "../../api/api";

export const setProductsAc = (products) => ({
    type: "SET_PRODUCT",
    payload: products,
  });


export const getProductsGameConsolesThunkCreator=()=>{
  return (dispatch)=>{
    productsAPI.getProductsGameConsoles().then((data) => {
     dispatch(setProductsAc(data));
    });
  }
}


