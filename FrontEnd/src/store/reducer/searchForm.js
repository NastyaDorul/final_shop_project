const InitialState = {
  searchText: "",
  searchIsReady: false,
  searchResult: null,
};
export default (state = InitialState, action) => {
  switch (action.type) {
    case "SET_SEARCH_QUERY":
      return {
        ...state,
        searchText: action.payload,
      };
    case "SET_SEARCH_QUERY_RESULT":
      return {
        ...state,
        searchResult: action.payload,
        searchIsReady: true,
      };

    default:
      return state;
  }
};
