const InitialState = {
  result: [],
  isReady: false,
  isReadyAfterRefresh: false,
  totalProductCount: null,
  currentPage: 1,
  limit: 12, //quantity of rendering
  selectedTypeOfSort: "SortBy is Empty",
  selectedColorFilter: "ColorFilter is Empty",
  selectedSizeFilter: "SizeFilter is Empty",
  selectedRange: {
    "20 to 49.99": false,
    "50 to 99.99": false,
    "100 to 499.99": false,
  },
  categoryPriceMin: "do not set",
  categoryPriceMax: "do not set",

  sortingItems: null,
  isCategorySorting: false,
  sortingColorItems: null,
  isColorSorting: false,
  sortingSizeItems: null,
  isSizeSorting: false,
  sortingPriceItems: null,
  isPriceSorting: false,
  sortingBrandItems: null,
  isBrandSorting: false,
  sortingGSItems: null,
  isGS: false, //Gaming System

  selectedBrand: {
    Midway: false,
    Atari: false,
    LucasArts: false,
    Namco: false,
    Nintendo: false,
    EASports: false,
    Sega: false,
    "Ubi Soft": false,
    "Rockstar Games": false,
    Sierra: false,
    Sony: false,
    Microsoft: false,
    No: false,
  },
  selectedGS: {
    "Sony PSP": false,
    Wii: false,
    "Nintendo DS": false,
    SonyPS3: false,
    "X-Box 360": false,
    No: false,
  },

  parentCategory: "",
  subCategory: "",

  currentItemDetails: {},
  itemDetails: "empty",
  itemDetailsIsReady: false,
  //-----------------------------------

  cartItems: [],
  newProductInCart: [],
  shoppingCartTotal: 0,
  shoppingCartCount: 0,

  //-----------------------------------

  currentConsoleItemId: "",
  ConsoleItem: "",
  consoleisReady: false,
};

const updateCartItems = (newProductInCart, item, idx) => {
  if (item.count === 0) {
    return [
      ...newProductInCart.slice(0, idx),
      ...newProductInCart.slice(idx + 1),
    ];
  }
  if (idx === -1) {
    return [...newProductInCart, item];
  }
  return [
    ...newProductInCart.slice(0, idx),
    item,
    ...newProductInCart.slice(idx + 1),
  ];
};
const updateCartItem = (addedProduct, item = {}, quantity) => {
  let newItem;
  const {
    id = addedProduct._id,
    count = 0,
    total = 0,
    title = addedProduct.title ? addedProduct.title : addedProduct.name,
    type = addedProduct.type,
    model = addedProduct.model,
    code = addedProduct.code,
    defaultImage = addedProduct.defaultImage,
    image = addedProduct.image ? addedProduct.image : addedProduct.images[0],
    genre = addedProduct.genre,
    gameRating = addedProduct.gameRating,
    description = addedProduct.description
      ? addedProduct.description
      : addedProduct.details,
    brand = addedProduct.brand,
    availability = addedProduct.availability,
  } = item;

  return {
    availability,
    brand,
    description,
    gameRating,
    description,
    genre,
    image,
    type,
    model,
    code,
    defaultImage,
    id,
    title,
    count: count + quantity,
    total: addedProduct.price
      ? quantity * addedProduct.price + total
      : quantity * addedProduct.details.price + total,
  };
};

const updateOrder = (state, addedProductId, quantity) => {
  const { result, cartItems } = state;
  const addedProduct = result.find(({ _id }) => _id === addedProductId);
  const itemIndex = cartItems.findIndex(({ id }) => id === addedProductId);
  const item = cartItems[itemIndex];

  const newItem = updateCartItem(addedProduct, item, quantity);
  return {
    ...state,
    cartItems: updateCartItems(cartItems, newItem, itemIndex),
  };
};

export default (state = InitialState, action) => {
  switch (action.type) {
    case "SET_SEARCH_RESULT":
      return {
        ...state,
        result: action.payload,
        isReady: true,
      };
    case "RESET_RESULT":
      return {
        ...state,
        result: action.payload,
        isReadyAfterRefresh: true,
      };
    case "SET_CURRENT_PAGE":
      return {
        ...state,
        currentPage: action.payload,
      };
    case "SET_PAGE_SIZE":
      return {
        ...state,
        pageSize: action.payload,
      };

    case "SET_TOTAL_RPODUCT_COUNT":
      return {
        ...state,
        totalProductCount: action.payload,
      };
    case "SET_PAGE_LIMIT":
      return {
        ...state,
        limit: action.payload,
      };
    case "SET_CURRENT_TYPE_OF_SORT":
      return {
        ...state,
        selectedTypeOfSort: action.payload,
      };
    case "SET_COLOR_FILTER":
      return {
        ...state,
        selectedColorFilter: action.payload,
      };
    case "SET_SIZE_FILTER":
      return {
        ...state,
        selectedSizeFilter: action.payload,
      };

    case "SET_PRICE_RANGE":
      return {
        ...state,
        selectedRange: action.payload,
      };
    case "SET_BRAND_FILTER":
      return {
        ...state,
        selectedBrand: action.payload,
      };
    case "SET_GAMING_SYSTEM_FILTER":
      return {
        ...state,
        selectedGS: action.payload,
      };

    case "SET_SUB_CATEGORY":
      return {
        ...state,
        subCategory: action.payload,
      };
    case "SET_PARENT_CATEGORY":
      return {
        ...state,
        parentCategory: action.payload,
      };
    case "SET_MIN":
      return {
        ...state,
        categoryPriceMin: action.payload,
      };
    case "SET_MAX":
      return {
        ...state,
        categoryPriceMax: action.payload,
      };
    case "SET_DETAILS_OF_ITEM":
      return {
        ...state,
        currentItemDetails: action.payload,
      };
    case "SET_ITEM":
      return {
        ...state,
        itemDetails: action.payload,
        itemDetailsIsReady: true,
      };
    case "SET_SORTING_ITEMS":
      return {
        ...state,
        sortingItems: action.payload,
        isCategorySorting: true,
      };
    case "SET_SORTING_COLOR":
      return {
        ...state,
        sortingColorItems: action.payload,
        isColorSorting: true,
      };
    case "SET_SORTING_SIZE":
      return {
        ...state,
        sortingSizeItems: action.payload,
        isSizeSorting: true,
      };
    case "SET_SORTING_PRICE":
      return {
        ...state,
        sortingPriceItems: action.payload,
        isPriceSorting: true,
      };
    case "SET_SORTING_BRAND":
      return {
        ...state,
        sortingBrandItems: action.payload,
        isBrandSorting: true,
      };
    case "SET_SORTING_GS":
      return {
        ...state,
        sortingGSItems: action.payload,
        isGS: true,
      };
    case "ADD_TO_CART":
      return updateOrder(state, action.payload, 1);

    case "REMOVE_FROM_CART":
      return updateOrder(state, action.payload, -1);

    case "REMOVE_ALL_FROM_CART":
      const item = state.cartItems.find((o) => o.id === action.payload);

      return updateOrder(state, action.payload, -item.count);

    case "SET_CURRENT_CONSOLE_ITEM_ID":
      return {
        ...state,
        currentConsoleItemId: action.payload,
      };
    case "SET_CURRENT_CONSOLE_ITEM":
      return {
        ...state,
        ConsoleItem: action.payload,
        consoleisReady: true,
      };

    default:
      return state;
  }
};
