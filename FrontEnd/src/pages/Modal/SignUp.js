import React,{useState} from "react";
import {Formik,Form,Field,ErrorMessage} from "formik";
import axios from "axios";
import * as Yup from "yup";
import "./modal.scss";

import TextError from "../pages/TextError";



const initialValues={
    email:"",
    yourname:"",
}

const validationSchema=Yup.object({
    email: Yup.string().email("Invalid email format")
    .required("Required"),
    yourname: Yup.string().typeError("Uncorrect personal data")
    .required("Required"),
})


function SignUpForm(props){
    
    const [serverState, setServerState] = useState();
    
    const [serverResponse,setResponse]=useState();

    console.log(serverState,serverResponse);


    const handleServerResponse = (ok, msg,email) => {
        setServerState({ok, msg});
        setResponse(email);
      };
    
    const onSubmit=(values,onSubmitProps)=>{

        console.log("Form data",values);
        console.log("onSubmittingProps",onSubmitProps);
        
        axios({
            method: "POST",
            url: "https://....",
            data: values
            
          })
          
          .then(response => {
            onSubmitProps.setSubmitting(false);
            onSubmitProps.resetForm();
            handleServerResponse(true, "Thanks!",response.data.email);
            console.log(response.data.email);
            
          })
          .catch(error => {
            onSubmitProps.setSubmitting(false);
            handleServerResponse(false, error.response.data.error);
            console.log("Error has occured");
          });
    
    }
  
return(
    <Formik 
    initialValues={initialValues}
    validationSchema={validationSchema}
    onSubmit={onSubmit} >
        {(formik)=>{
            return(
                
                <Form>
                <div className="inner">
                 <div className="err-wrapper"><div className="field-wrapper">
                <div>
                <Field 
                className="input"
                 placeholder="daine@email.com"
                 type="text" 
                 id="email" 
                 name="email" 
                 />
                 </div></div>
                 <ErrorMessage name="email" component={TextError}/></div>
                 

                 <div className="err-wrapper"><div className="field-wrapper">
                <div>
                <Field 
                className="input"
                 placeholder="Name"
                 type="text" 
                 id="yourname" 
                 name="yourname" 
                 />
                 </div></div>
                 <ErrorMessage name="yourname" component={TextError}/></div>
                 
               
                <div className="btn-wrapper">
                <button className="submit-btn" disabled={!formik.isValid || formik.isSubmitting} type="submit">Ja, ich möchte!</button>
                </div></div>
            </Form>
            )
        }}
       
    </Formik>

    
)
}
export default SignUpForm;
