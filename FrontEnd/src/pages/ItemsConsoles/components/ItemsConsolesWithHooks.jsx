import React,{useEffect} from "react";
import Preloader from "../../../components/Preloader/index";
import PropTypes from "prop-types";

const ItemsConsoles =({ consoleisReady, ConsoleItem,currentConsoleItemId,
  setCurrentConsoleItemThunk })=>{

console.log(typeof(ConsoleItem),consoleisReady);
  useEffect(()=>{
    setCurrentConsoleItemThunk(currentConsoleItemId);
  },[currentConsoleItemId]);

  return (
    <div className="m-4 d-flex justify-items-center">
      <ul>
        <h3>Information about item</h3>
        {!consoleisReady ? (
          <Preloader />
        ) : (
          <li>
            <h3>{ConsoleItem.title}</h3>
            <h3>Availability: {ConsoleItem.availability}</h3>
            <img src={ConsoleItem.image} alt="availability" />

            <h3>Brand: {ConsoleItem.brand}</h3>
            <h3>Extended Warranty:{ConsoleItem.extendedWarranty}</h3>
            <h3>Price:{ConsoleItem.price} </h3>
            <h3>{ConsoleItem.description}</h3>
          </li>
        )}
      </ul>
    </div>
  );

}

export default ItemsConsoles;

ItemsConsoles.propTypes={
  consoleisReady:PropTypes.bool,
  ConsoleItem:PropTypes.object,//initially string in store
  currentConsoleItemId:PropTypes.string,
  setCurrentConsoleItemThunk:PropTypes.func,
}
