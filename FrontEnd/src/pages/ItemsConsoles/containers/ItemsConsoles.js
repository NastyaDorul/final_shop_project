import React from "react";
import { useSelector,useDispatch } from "react-redux";
import {
  getCurrentConsoleItemThunkCreator,
} from "../../../store/actions/Results";

import ItemsConsoles from "../components/ItemsConsolesWithHooks";

function ItemsConsolesContainer(){

  const currentConsoleItemId=useSelector((state)=>state.result.currentConsoleItemId);
  const ConsoleItem=useSelector((state)=>state.result.ConsoleItem);
  const consoleisReady=useSelector((state)=>state.result.consoleisReady);

  const dispatch=useDispatch();
  const setCurrentConsoleItemThunk=(currentConsoleItemId)=>dispatch(getCurrentConsoleItemThunkCreator(currentConsoleItemId)); 


return(
<ItemsConsoles
currentConsoleItemId={currentConsoleItemId}
ConsoleItem={ConsoleItem}
consoleisReady={consoleisReady}
setCurrentConsoleItemThunk={setCurrentConsoleItemThunk}
/>);

}


export default ItemsConsolesContainer;
