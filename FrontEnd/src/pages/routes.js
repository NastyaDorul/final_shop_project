import React from "react";

import { Switch, Route, Redirect } from "react-router-dom";
import { LinksPage } from "../pages/Auth_Login/LinksPage";
import { CreatePage } from "../pages/Auth_Login/CreatePage";
import { DetailPage } from "../pages/Auth_Login/DetailPage";
import { AuthPage } from "../pages/Auth_Login/AuthPage";
import ResultsConsolesContainer from "../pages/ResultsConsoles/containers";
import ResultContainer from "../pages/Results/container/Results";
import ItemsConsoles from "../pages/ItemsConsoles/containers/ItemsConsoles";
import ItemsDetalizationContainer from "../pages/ItemsDetalization/containers/ItemDetalizationWithHooks";
//import ItemsDetalizationContainer from "../pages/ItemsDetalization/containers/ItemsDetalization";
import WellcomePageItem from "../pages/WellcomePageProfile/container/WellcomePageItem";
import ItemsSearchForm from "../../src/pages/ ItemsSearchForm/container";
import ShoppingCartTable from "../pages/CardPage/container/index";


export const useRoutes = (isAuthenticated) => {
  if (isAuthenticated) {
    return (
      <Switch>
        <Route exact path="/links">
          <LinksPage />
        </Route>
        <Route exact path="/create">
          <CreatePage />
        </Route>
        <Route path="/detail/:id">
          <DetailPage />
        </Route>
        <Route exact path="/" render={() => <ResultsConsolesContainer />} />

        <Route
          exact
          path="/category?parentCategory=electronics&subCategory=gaming"
          render={() => <ResultsConsolesContainer />}
        />

        {/*route for start product list */}
        <Route exact
          path="/parentCategory/"
          render={() => <ResultContainer />}
        />
        {/*<Results />*/}
        {/*route for detail info of product item */}
        <Route
          exact
          path="/products/gaming-Game-consoles/search-By/:id"
          render={() => <ItemsConsoles />}
        />
        {/*route for detail info of product item*/}
        <Route
          exact
          path="/products/detalization/:id"
          render={() => <ItemsDetalizationContainer />}
        />

        {/*route for detail info of product item*/}
        <Route
          exact
          path="/products/subCategory/"
          render={() => <WellcomePageItem />}
        />

        {/*route for search result page*/}
        <Route
          exact
          path="/search/products/:id"
          render={() => <ItemsSearchForm />}
        />
        {/*route for Cart Page*/}
        <Route
          exact
          path="/products/cart"
          render={() => <ShoppingCartTable />}
        />

        {/*route for Wellcome Page*/}
        <Route exact path="/category/" render={() => <WellcomePageItem />} />
        <Redirect to="/create" />
      </Switch>
    );
  }
  return (
    <Switch>
      <Route exact path="/auth">
        <AuthPage />
      </Route>

      <Route exact path="/" render={() => <ResultsConsolesContainer />} />

      <Route
        exact
        path="/category?parentCategory=electronics&subCategory=gaming"
        render={() => <ResultsConsolesContainer />}
      />

      {/*route for start product list */}
      <Route exact path="/parentCategory/" render={() => <ResultContainer />} />
      {/*<Results />*/}
      {/*route for detail info of product item */}
      <Route
        exact
        path="/products/gaming-Game-consoles/search-By/:id"
        render={() => <ItemsConsoles />}
      />
      {/*route for detail info of product item*/}
      <Route
        exact
        path="/products/detalization/:id"
        render={() => <ItemsDetalizationContainer />}
      />

      {/*route for detail info of product item*/}
      <Route
        exact
        path="/products/subCategory/"
        render={() => <WellcomePageItem />}
      />

      {/*route for search result page*/}
      <Route
        exact
        path="/search/products/:id"
        render={() => <ItemsSearchForm />}
      />
      {/*route for Cart Page*/}
      <Route exact path="/products/cart" render={() => <ShoppingCartTable />} />

      {/*route for Wellcome Page*/}
      <Route exact path="/category/" render={() => <WellcomePageItem />} />
      <Redirect to="/auth" />
    </Switch>
  );
};
