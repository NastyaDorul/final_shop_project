import React from "react";
import PropTypes from "prop-types";

const ColorSelectComponent = ({categoryColors,onColorChanged}) => {
  return (
    <div>
      <div
        data-toggle="tooltip"
        data-placement="right"
        title={categoryColors}
        style={{
          width: "40px",
          height: "40px",
          border: "1px solid lightgrey",
          backgroundColor: categoryColors,
          borderRadius: "10px",
          margin: "2px",
        }}
        onClick={(e, selectedColorFilter) => {
          onColorChanged(e.target.title, selectedColorFilter);
        }}
      ></div>
    </div>
  );
};

export default ColorSelectComponent;

ColorSelectComponent.propTypes={
  categoryColors:PropTypes.string,
  onColorChanged:PropTypes.func.isRequired
}
