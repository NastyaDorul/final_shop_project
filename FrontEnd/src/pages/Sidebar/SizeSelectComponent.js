import React from "react";
import PropTypes from "prop-types";

const SizeSelectComponent = ({categorySize,onSizeChanged}) => {
  return (
    <div>
      <div
        className="container justify-content-center"
        data-toggle="tooltip"
        data-placement="right"
        title={categorySize}
        style={{
          width: "50px",
          height: "30px",
          border: "1px solid grey",
          backgroundColor: "white",
          borderRadius: "5px",
          margin: "2px",
        }}
        onClick={(e, selectedSizeFilter) =>
          onSizeChanged(e.currentTarget.title, selectedSizeFilter)
        }
      >
        <p
          style={{
            color: "grey",
            fontSize: 16,
            fontFamily: "Arial",
          }}
        >
          {categorySize}
        </p>
      </div>
    </div>
  );
};

export default SizeSelectComponent;

SizeSelectComponent.propTypes={
  categorySize:PropTypes.string,
  onSizeChanged:PropTypes.func.isRequired,
}
