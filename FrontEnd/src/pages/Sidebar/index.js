import React from "react";
import { Link } from "react-router-dom";
import {useSelector,useDispatch} from "react-redux";
import ColorSelectComponent from "./ColorSelectComponent";
import SizeSelectComponent from "./SizeSelectComponent";
import PriceSelectComponent from "./PriceSelectComponent";
import BrandSelectComponent from "./BrandSelectComponent";
import GamingSystemSelectComponent from "./GamingSystemSelectComponent";
import CategoriesSelectComponent from "./CategoriesSelectComponent";
//import SearchFormContainer from "../SearchForm/containers/SearchForm";
import SearchFormContainer from "../SearchForm/containers/SearchForm";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Preloader from "../../components/Preloader";

import {getProductsOnPriceRangeChangedThunkCreator,setPriceRangeAc,setGSFilterAc,
  setSubCategoryAc,getProductsOnChangeCategoryThunkCreator,setMaxAc,setMinAc,
  setColorFilterAc,getProductsThunkCreator,setSizeFilterAc,setBrandFilterAc,} from "../../store/actions/Results"

const SidebarComponent = (
  { CategoryItems, BrandItems, SizeItems, ColorItems, GamingSystemItems }
) => {

  const categoryColorName = "color";
  const сategorySizeName = "size";
  const categoryPriceName = "price";
  const categoryBrandName = "brand";
  const categoryGamingSystemName = "gaming system";

  const PriceArrey = [
    { pmin: 20, pmax: 49.99 },
    { pmin: 50, pmax: 99.99 },
    { pmin: 100, pmax: 499.99 },
  ];

  
  const selectedRange=useSelector((state)=>state.result.selectedRange); 
  const parentCategory=useSelector((state)=>state.result.parentCategory);
  const limit=useSelector(state=>state.result.limit);
  const currentPage=useSelector(state=>state.result.currentPage);
  const selectedTypeOfSort=useSelector(state=>state.result.selectedTypeOfSort);
  const selectedColorFilter=useSelector(state=>state.result.selectedColorFilter);
  const selectedSizeFilter=useSelector(state=>state.result.selectedSizeFilter);
  const selectedBrand=useSelector(state=>state.result.selectedBrand);
  const selectedGS=useSelector((state)=>state.result.selectedGS);
  const subCategory=useSelector((state)=>state.result.subCategory);
 
  const categoryPriceMin=useSelector((state)=>state.result.categoryPriceMin);
  const categoryPriceMax=useSelector((state)=>state.result.categoryPriceMax);
  const sortingItems=useSelector((state)=>state.result.sortingItems);
  const sortingBrandItems=useSelector((state)=>state.result.sortingBrandItems);
  const sortingSizeItems=useSelector((state)=>state.result.sortingSizeItems);
  const sortingColorItems=useSelector((state)=>state.result.sortingColorItems);
  const sortingGSItems=useSelector((state)=>state.result.sortingGSItems);

  const isColorSorting=useSelector((state)=>state.result.isColorSorting);
  const isCategorySorting=useSelector((state)=>state.result.isCategorySorting);
  const isBrandSorting=useSelector((state)=>state.result.isBrandSorting);
  const isSizeSorting=useSelector((state)=>state.result.isSizeSorting);
  const isGS=useSelector((state)=>state.result.isGS);

  const dispatch=useDispatch();
  const setColorFilter=(selectedColorFilter) => dispatch(setColorFilterAc(selectedColorFilter)); 
  const setProductsThunk=(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter)=>{
    dispatch(getProductsThunkCreator(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter));
};
  const setSizeFilter=(selectedSizeFilter) => dispatch(setSizeFilterAc(selectedSizeFilter)); 
  const setBrandFilter=(selectedBrand) => dispatch(setBrandFilterAc(selectedBrand));
  const setPriceRange=(selectedRange) => dispatch(setPriceRangeAc(selectedRange));
  const setProductsOnPriceRangeChangedThunk=(parentCategory,currentPage,limit,
    selectedTypeOfSort,selectedColorFilter,selectedSizeFilter,
    test,selectedCategory)=>{
    dispatch(getProductsOnPriceRangeChangedThunkCreator(parentCategory,currentPage,limit,
      selectedTypeOfSort,selectedColorFilter,selectedSizeFilter,
      test,selectedCategory));
  }
  const setGSFilter=(selectedGS)=>dispatch(setGSFilterAc(selectedGS)); 
  const setSubCategory=(subCategory)=>dispatch(setSubCategoryAc(subCategory));
  const setProductsOnChangeCategoryThunk=(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter,subCategory)=>{
    dispatch(getProductsOnChangeCategoryThunkCreator(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter,subCategory));
  }
  const setMin=(categoryPriceMin)=>dispatch(setMinAc(categoryPriceMin));
  const setMax=(categoryPriceMax)=>dispatch(setMaxAc(categoryPriceMax));

  const onPriceRangeChanged = (selectedRange, min, max) => {
    setPriceRange(selectedRange);
    console.log(selectedRange);
    const test = [];
    for (let key in selectedRange) {
      if (selectedRange[key] === true) {
        test.push(key);
        // min = String(test).split(" ")[0];
        // max = String(test).split(" ")[2];
        // console.log(min, max);
      }
    }
    let test1 = String(test);
    //let test1 = String(test).splice(2, [, 1, "&priceRange="]);do not working
    console.log(test1);
    setProductsOnPriceRangeChangedThunk(parentCategory,currentPage,limit,
      selectedTypeOfSort,selectedColorFilter,selectedSizeFilter,
      test) //was before  test,selectedCategory,use subCategory
  };

  const  onColorChanged = (selectedColorFilter) => {
    setColorFilter(selectedColorFilter);
    console.log(selectedColorFilter);
    setProductsThunk(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter);
  };

  const  onSizeChanged = (selectedSizeFilter) => {
    setSizeFilter(selectedSizeFilter);
    console.log(selectedSizeFilter);
    setProductsThunk(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter);
  };

  const onBrandChanged = (selectedBrand) => {
    setBrandFilter(selectedBrand);
    console.log(selectedBrand);
  };

  const  onGamingSystemChanged = (selectedGS) => {
    setGSFilter(selectedGS);
    console.log(selectedGS);
  };

  const onCategoryChanged = (subCategory) => {
    setSubCategory(subCategory);
    console.log(subCategory);
    setProductsOnChangeCategoryThunk(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter,subCategory);
  };

  const onMinChanged = (categoryPriceMin) => {
    setMin(categoryPriceMin);
    console.log(categoryPriceMin);
  };
  const onMaxChanged = (categoryPriceMax) => {
    setMax(categoryPriceMax);
    console.log(categoryPriceMax);
  };


  {
    sortingItems !== null &&
      (CategoryItems = sortingItems.map((category, index) => {
        return (
          <div key={index} id="category" className="collapse">
            <CategoriesSelectComponent
              category={category}
              onCategoryChanged={onCategoryChanged}
            />
          </div>
        );
      }));
  }
  {
    sortingColorItems !== null &&
      (ColorItems = sortingColorItems.map((elem, index) => {
        return (
          <div key={index} id="color" className="collapse">
            <ColorSelectComponent
              categoryColors={elem}
              onColorChanged={onColorChanged}
            />
          </div>
        );
      }));
  }

  {
    sortingSizeItems !== null &&
      (SizeItems = sortingSizeItems.map((size, index) => {
        return (
          <div key={index} id="size" className="collapse">
            <SizeSelectComponent
              categorySize={size}
              onSizeChanged={onSizeChanged}
            />
          </div>
        );
      }));
  }

  let PriceItems = PriceArrey.map((price, index) => {
    return (
      <div key={index} id="price" className="collapse">
        <PriceSelectComponent
          categoryPriceMax={price.pmax}
          categoryPriceMin={price.pmin}
          onPriceRangeChanged={onPriceRangeChanged}
          onMinChanged={onMinChanged}
          selectedRange={selectedRange}
          onMaxChanged={onMaxChanged}
        />
      </div>
    );
  });

  {
    sortingBrandItems !== null &&
      (BrandItems = sortingBrandItems.map((brand, index) => {
        return (
          <div key={index} id="brand" className="collapse">
            <BrandSelectComponent
              categoryName={brand}
              onBrandChanged={onBrandChanged}
              selectedBrand={selectedBrand}
            />
          </div>
        );
      }));
  }

  {
    sortingGSItems !== null &&
      (GamingSystemItems = sortingGSItems.map((system, index) => {
        return (
          <div key={index} id="system" className="collapse">
            <GamingSystemSelectComponent
              categoryName={system}
              onGamingSystemChanged={onGamingSystemChanged}
              selectedGS={selectedGS}
            />
          </div>
        );
      }));
  }

  return (
    <div>
      <div className="card" style={{ width: "220px" }}>
        <div className="card-header" />
        <nav className="navbar">
          {" "}
          <SearchFormContainer />
        </nav>
      </div>

      {!isCategorySorting ? (
        <Preloader />
      ) : (
        <div className="card" style={{ width: "220px" }}>
          <div className="card-header">
            <Link
              to=""
              className="card-link"
              style={{ fontWeight: "bold", color: "black" }}
              data-toggle="collapse"
              data-target="#category"
            >
              <FontAwesomeIcon
                icon={faCheckCircle}
                size="x"
                style={{ color: "darkgreen", marginRight: "10px" }}
              />
              {parentCategory.toUpperCase()}{" "}
            </Link>{" "}
          </div>
          <Link
            to={`/products/subCategory?${subCategory}`}
            style={{ textDecoration: "none", color: "black" }}
          >
            {" "}
            <nav className="navbar">{CategoryItems}</nav>
          </Link>
        </div>
      )}

      {sortingColorItems == null ? (
        <></>
      ) : (
        <div>
          {!isColorSorting ? (
            <Preloader />
          ) : (
            <div className="card" style={{ width: "220px" }}>
              <div className="card-header">
                <Link
                  to=""
                  className="card-link"
                  style={{ fontWeight: "bold", color: "black" }}
                  data-toggle="collapse"
                  data-target="#color"
                >
                  <FontAwesomeIcon
                    icon={faCheckCircle}
                    size="x"
                    style={{ color: "darkgreen", marginRight: "10px" }}
                  />
                  {categoryColorName.toUpperCase()}
                </Link>{" "}
              </div>
              <Link to={`/products/subCategory?${subCategory}`}>
                {" "}
                <nav className="navbar">{ColorItems}</nav>
              </Link>
            </div>
          )}
        </div>
      )}

      {sortingSizeItems == null ? (
        <></>
      ) : (
        <div>
          {!isSizeSorting ? (
            <Preloader />
          ) : (
            <div className="card" style={{ width: "220px" }}>
              <div className="card-header">
                <Link
                  to=""
                  className="card-link"
                  style={{ fontWeight: "bold", color: "black" }}
                  data-toggle="collapse"
                  data-target="#size"
                >
                  <FontAwesomeIcon
                    icon={faCheckCircle}
                    size="x"
                    style={{ color: "darkgreen", marginRight: "10px" }}
                  />
                  {сategorySizeName.toUpperCase()}
                </Link>{" "}
              </div>
              <nav className="navbar">{SizeItems}</nav>
            </div>
          )}
        </div>
      )}

      <div className="card" style={{ width: "220px" }}>
        <div className="card-header">
          <Link
            to=""
            className="card-link"
            style={{ fontWeight: "bold", color: "black" }}
            data-toggle="collapse"
            data-target="#price"
          >
            <FontAwesomeIcon
              icon={faCheckCircle}
              size="x"
              style={{ color: "darkgreen", marginRight: "10px" }}
            />
            {categoryPriceName.toUpperCase()}
          </Link>{" "}
        </div>
        <nav className="navbar">{PriceItems}</nav>
      </div>

      {sortingBrandItems == null ? (
        <></>
      ) : (
        <div>
          {!isBrandSorting ? (
            <Preloader />
          ) : (
            <div className="card" style={{ width: "220px" }}>
              <div className="card-header">
                <Link
                  to=""
                  className="card-link"
                  style={{ fontWeight: "bold", color: "black" }}
                  data-toggle="collapse"
                  data-target="#brand"
                >
                  <FontAwesomeIcon
                    icon={faCheckCircle}
                    size="x"
                    style={{ color: "darkgreen", marginRight: "10px" }}
                  />
                  {categoryBrandName.toUpperCase()}
                </Link>{" "}
              </div>
              <nav className="navbar">{BrandItems}</nav>
            </div>
          )}
        </div>
      )}

      {sortingGSItems == null ? (
        <></>
      ) : (
        <div>
          {!isGS ? (
            <Preloader />
          ) : (
            <div className="card" style={{ width: "220px" }}>
              <div className="card-header">
                <Link
                  to=""
                  className="card-link"
                  style={{ fontWeight: "bold", color: "black" }}
                  data-toggle="collapse"
                  data-target="#system"
                >
                  <FontAwesomeIcon
                    icon={faCheckCircle}
                    size="x"
                    style={{ color: "darkgreen", marginRight: "10px" }}
                  />
                  {categoryGamingSystemName.toUpperCase()}
                </Link>{" "}
              </div>
              <Link
                to={`/products/subCategory?${subCategory}`}
                style={{ textDecoration: "none", color: "black" }}
              >
                <nav className="navbar">{GamingSystemItems}</nav>
              </Link>
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default SidebarComponent;
