import React from "react";
import PropTypes from "prop-types";


const CategoriesSelectComponent = ({category,onCategoryChanged}) => {
  return (
    <div>
      <div
        className="container"
        title={category}
        style={{
          width: "180px",
          height: "30px",
          margin: "2px",
        }}
        onClick={(e, subCategory) =>
          onCategoryChanged(e.currentTarget.title, subCategory)
        }
      >
        <p>{category}</p>
      </div>
    </div>
  );
};

export default CategoriesSelectComponent;

CategoriesSelectComponent.propTypes={
  category:PropTypes.string,
  onCategoryChanged:PropTypes.func.isRequired
}