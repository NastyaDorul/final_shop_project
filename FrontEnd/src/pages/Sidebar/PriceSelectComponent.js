import React from "react";
import PropTypes from "prop-types";

const PriceSelectComponent = ({categoryPriceMax,categoryPriceMin,selectedPriceMax,selectedRange,
  onPriceRangeChanged,onMinChanged,onMaxChanged,}) => {
  return (
    <div>
      <div>
        <div
          className="container"
          data-toggle="tooltip"
          data-placement="left"
          title={categoryPriceMax}
          style={{
            width: "150px",
            height: "30px",
            border: "none",
            backgroundColor: "white",
            margin: "2px",
          }}
        >
          <input
            type="checkbox"
            checked={selectedPriceMax}
            className="form-check-input"
            value={`${categoryPriceMin} to ${categoryPriceMax}`}
            onChange={(e) => {
              console.log(typeof(selectedPriceMax));
              let object = Object.assign(selectedRange);
              object[e.currentTarget.value] = e.currentTarget.checked;
              onPriceRangeChanged(object);
              onMinChanged(categoryPriceMin);
              onMaxChanged(categoryPriceMax);
            }}
          />
          {`${categoryPriceMin} to ${categoryPriceMax}`}
        </div>
      </div>
    </div>
  );
};

export default PriceSelectComponent;

PriceSelectComponent.propTypes={
  categoryPriceMax:PropTypes.number,
  categoryPriceMin:PropTypes.number,
  selectedPriceMax:PropTypes.number,
  selectedRange:PropTypes.object,
  onPriceRangeChanged:PropTypes.func.isRequired,
  onMinChanged:PropTypes.func.isRequired,
  onMaxChanged:PropTypes.func.isRequired
}
