import React from "react";
import PropTypes from "prop-types";

const BrandSelectComponent = ({categoryName,selectedPriceMax,selectedBrand,onBrandChanged}) => {
  return (
    <div>
      <div>
        <div
          className="container"
          data-toggle="tooltip"
          data-placement="left"
          title={categoryName}
          style={{
            width: "150px",
            height: "30px",
            border: "none",
            backgroundColor: "white",
            margin: "2px",
          }}
        >
          <input
            type="checkbox"
            checked={selectedPriceMax} //other value
            className="form-check-input"
            value={categoryName}
            onChange={(e) => {
              let object = Object.assign(selectedBrand);
              console.log(object);
              object[e.currentTarget.value] = e.currentTarget.checked;
              onBrandChanged(object);
            }}
          />
          {categoryName}
        </div>
      </div>
    </div>
  );
};

export default BrandSelectComponent;

BrandSelectComponent.propTypes={
  categoryName:PropTypes.string,
  selectedPriceMax:PropTypes.number,
  selectedBrand:PropTypes.object,
  onBrandChanged:PropTypes.func.isRequired
}
