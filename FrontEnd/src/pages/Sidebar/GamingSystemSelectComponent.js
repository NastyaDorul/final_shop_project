import React from "react";
import PropTypes from "prop-types";

const GamingSystemSelectComponent = ({categoryName,selectedPriceMax,selectedGS,onGamingSystemChanged}) => {
  return (
    <div>
      <div>
        <div
          className="container"
          data-toggle="tooltip"
          data-placement="left"
          title={categoryName}
          style={{
            width: "150px",
            height: "30px",
            border: "none",
            backgroundColor: "white",
            margin: "2px",
          }}
        >
          <input
            type="checkbox"
            checked={selectedPriceMax} //other value
            className="form-check-input"
            value={categoryName}
            onChange={(e) => {
              console.log(selectedGS);
              let object = Object.assign(selectedGS);
              console.log(object);
              object[e.currentTarget.value] = e.currentTarget.checked;
              onGamingSystemChanged(object);
            }}
          />
          {categoryName}
        </div>
      </div>
    </div>
  );
};

export default GamingSystemSelectComponent;

GamingSystemSelectComponent.propTypes={
  categoryName:PropTypes.string,
  selectedPriceMax:PropTypes.number,
  selectedGS:PropTypes.object,
  onGamingSystemChanged:PropTypes.func.isRequired
}
