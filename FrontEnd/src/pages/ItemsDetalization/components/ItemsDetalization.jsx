import React from "react";
import Preloader from "../../../components/Preloader/index";
import PropTypes from "prop-types";

const ItemsDetalization = ({ itemDetails, itemDetailsIsReady, parentCategory }) => {
  
  console.log(typeof(itemDetailsIsReady));
  return (
    <div className="m-4 d-flex justify-items-center">
      <ul>
        <h3>Information about item</h3>
        {!itemDetailsIsReady ? (
          <Preloader />
        ) : (
          <li>
            {parentCategory === "electronics" && (
              <>
                {" "}
                <h3>{itemDetails.title}</h3>
                <h3>Availability: {itemDetails.availability}</h3>
                <img src={itemDetails.image} alt="" />
                <h3>Brand: {itemDetails.brand}</h3>
                <h3>Extended Warranty: {itemDetails.extendedWarranty}</h3>
                <h3>Price: {itemDetails.price} </h3>
                <h3>Description: {itemDetails.description}</h3>
              </>
            )}
            {parentCategory === "womans" && (
              <>
                {" "}
                <h3>Назва товару: {itemDetails.name}</h3>
                <h3>Бренд: {itemDetails.brand}</h3>
                <h3>Код товару: {itemDetails.code}</h3>
                <img src={itemDetails.images[0]} alt="" />
                <h3>Модель: {itemDetails.model}</h3>
                <h3>Ціна: {itemDetails.details.price} </h3>
                <h3>Колір: {itemDetails.details.color}</h3>
                <h3>Матеріал: {itemDetails.details.material}</h3>
                <h3>Покриття: {itemDetails.details.lining}</h3>
                <h3>Сезон: {itemDetails.details.season}</h3>
                <h3>Підошва: {itemDetails.details.sole}</h3>
                <h3>Країна-виробник: {itemDetails.details.producingcountry}</h3>
                <h3>Розмір: {itemDetails.details.size}</h3>
              </>
            )}
          </li>
        )}
      </ul>
    </div>
  );
};
export default ItemsDetalization;

ItemsDetalization.propTypes={
  itemDetails: PropTypes.object,//initially string in store
  itemDetailsIsReady:PropTypes.bool, 
  parentCategory:PropTypes.string,
}