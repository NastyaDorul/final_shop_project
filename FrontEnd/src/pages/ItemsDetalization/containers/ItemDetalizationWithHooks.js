import React,{useEffect} from "react";
import { useSelector,useDispatch } from "react-redux";
import ItemsDetalization from "../components/ItemsDetalization";
import { getItemDetailsThunkCreator, } from "../../../store/actions/Results";



function ItemsDetalizationContainer(id,parentCategory) {
  
id=useSelector(state=>state.result.currentItemDetails.id);
parentCategory=useSelector(state=>state.result.parentCategory);
const itemDetails=useSelector(state=>state.result.itemDetails);
const itemDetailsIsReady=useSelector(state=>state.result.itemDetailsIsReady);


const dispatch=useDispatch();
const setItemDetailsThunk=(parentCategory,id)=>dispatch(getItemDetailsThunkCreator(parentCategory,id));

 
  useEffect(()=>{
    setItemDetailsThunk(parentCategory,id);
  },[id,parentCategory]);
 
    return(
        <ItemsDetalization 
        itemDetails={itemDetails}
        itemDetailsIsReady={itemDetailsIsReady}
        parentCategory={parentCategory}
        />
        )  
}

export default ItemsDetalizationContainer;
