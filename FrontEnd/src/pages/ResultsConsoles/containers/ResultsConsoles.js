
import React,{ useCallback } from "react";
import { useSelector,useDispatch } from "react-redux";
import {getProductsGameConsolesThunkCreator} from "../../../store/actions/products";
import ResultsConsoles from "../components/ResultsConsoles";


function ResultsConsolesContainer() {

  const products=useSelector((state)=>state.products.items);
  const isReady=useSelector((state)=>state.products.isReady);
  const currentConsoleItemId=useSelector((state)=>state.result.currentConsoleItemId);
  const ConsoleItem=useSelector((state)=>state.result.ConsoleItem);
 
  
  const dispatch=useDispatch();
  const setProductsGameConsolesThunk=(()=>dispatch(getProductsGameConsolesThunkCreator()));
  const setCurrentConsoleItemId=useCallback((currentConsoleItemId)=>dispatch({type: "SET_CURRENT_CONSOLE_ITEM_ID",
  payload: currentConsoleItemId}),[]);
  const setCurrentConsoleItem=useCallback((ConsoleItem)=>dispatch({type: "SET_CURRENT_CONSOLE_ITEM",
  payload: ConsoleItem}),[]); 

  return(
    <ResultsConsoles products={products}
    isReady={isReady}
    currentConsoleItemId={currentConsoleItemId}
    ConsoleItem={ConsoleItem}
    setProductsGameConsolesThunk={setProductsGameConsolesThunk}
    setCurrentConsoleItemId={setCurrentConsoleItemId}
    setCurrentConsoleItem={setCurrentConsoleItem}
    />
  );
}


export default ResultsConsolesContainer;
