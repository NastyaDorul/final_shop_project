import React,{useEffect} from "react";
import PropTypes from "prop-types";
import Preloader from "../../../components/Preloader/index";
import Card from "../../CardPageConsoles/index";


const ResultsConsoles=({setProductsGameConsolesThunk,
  products,
  isReady,
  currentConsoleItemId,
  setCurrentConsoleItemId,
  currentConsoleItem,
  setCurrentConsoleItem})=>{

    useEffect(()=>{
      setProductsGameConsolesThunk();
    },[]);


    return (
      <div className="m-5 p-5 container-fluid d-flex flex-wrap justify-content-center bg-light">
        {!isReady ? (
          <Preloader />
        ) : (
          products.map((product, i) => (
            <div key={i} className="row">
              <div className="col-md-1">
                <Card
                  title={product.title}
                  link={product.image}
                  price={product.price}
                  id={product._id}
                  setCurrentConsoleItemId={setCurrentConsoleItemId}
                  currentConsoleItemId={currentConsoleItemId}
                  currentConsoleItem={currentConsoleItem}
                  setCurrentConsoleItem={setCurrentConsoleItem}
                />
              </div>
            </div>
          ))
        )}
      </div>
    );
  }

export default ResultsConsoles;

ResultsConsoles.propTypes={
  setProductsGameConsolesThunk:PropTypes.func.isRequired,
  products:PropTypes.array,
  isReady:PropTypes.bool,
}

Card.propTypes={
  currentConsoleItemId:PropTypes.string,
  setCurrentConsoleItemId:PropTypes.func.isRequired,
  currentConsoleItem:PropTypes.object,
  setCurrentConsoleItem:PropTypes.func.isRequired,
}
