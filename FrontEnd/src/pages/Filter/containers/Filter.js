import {useCallback} from "react";
import { useSelector,useDispatch } from "react-redux";
import Filter from "../components/Filter";


function FilterContainer(){
  const filterBy=useSelector((state)=>state.products.filterBy);

  const dispatch=useDispatch();
  const setFilter=useCallback((filter)=>dispatch({type: "SET_FILTER",
  payload: filter}),[]);

  return(
    <Filter filterBy={filterBy}
    setFilter={setFilter}/>
  );
}

export default FilterContainer;
