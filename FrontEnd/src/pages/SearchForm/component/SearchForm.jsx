import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PropTypes from "prop-types";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

const SearchForm = ({setSearchQuery,searchText,onSearchTextChanged,
  parentCategory,}) => {
  
  return (
    <div>
      <input
        onChange={(e) => {
          let searchText = e.target.value;
          setSearchQuery(searchText);
        }}
        value={searchText}
        type="text"
        placeholder="Enter search-key..."
        style={{ width: "150px" }}
      />
      <button
        onClick={() => {
          if (searchText !=="") {
            onSearchTextChanged(parentCategory,searchText);
          }
        }}
      >
        <Link to={`/search/products/${searchText}`}>
          <FontAwesomeIcon icon={faSearch} style={{ color: "grey" }} />
        </Link>
      </button>
    </div>
  );
};

export default SearchForm;

SearchForm.propTypes={
  setSearchQuery:PropTypes.func.isRequired,
  searchText:PropTypes.string,
  onSearchTextChanged:PropTypes.func,
  parentCategory:PropTypes.string 
}
