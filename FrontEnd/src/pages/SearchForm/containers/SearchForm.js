import React from "react";
import { useSelector,useDispatch } from "react-redux";
import {
  setSearchQueryAc,
  getProductsOnSearchTextChangedThunkCreator,
} from "../../../store/actions/searchForm";
import SearchForm from "../component/SearchForm";

const SearchFormContainer=()=> {

const searchText=useSelector((state)=>state.search.searchText);
const searchIsReady=useSelector((state)=>state.search.searchIsReady);
const searchResult=useSelector((state)=>state.search.searchResult);
const parentCategory=useSelector((state)=>state.result.parentCategory);

const dispatch=useDispatch();
const setSearchQuery=(searchText) => dispatch(setSearchQueryAc(searchText));
const setProductsOnSearchTextChangedThunk=(parentCategory,searchText)=>dispatch(getProductsOnSearchTextChangedThunkCreator(parentCategory,searchText))

const  onSearchTextChanged = (parentCategory,searchText) => {
    console.log(searchText);
    setProductsOnSearchTextChangedThunk(parentCategory,searchText);
  };  
    return (
      <SearchForm
        searchIsReady={searchIsReady}
        searchResult={searchResult}
        parentCategory={parentCategory}
        searchText={searchText}
        setSearchQuery={setSearchQuery}
        onSearchTextChanged={onSearchTextChanged}
      />
    );
  }

export default SearchFormContainer;
