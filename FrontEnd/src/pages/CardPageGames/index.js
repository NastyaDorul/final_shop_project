import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import "./style.scss";

const CardElectronics = ({id,title,link,price,currentItemDetails,onItemClick,onAdd}) => {
  
  return (
    <div style={{ width: "20rem" }}>
      <div className=" d-flex  align-items-end justify-content-center card-deck border shadow m-0 p-0 ">
        <Link
          to={`/products/detalization/${id}`}
          className="btn stretched-link"
          data-title={title}
          data-img={link}
          data-price={price}
          data-id={id}
          onClick={(e) => {
            let object = Object.assign(currentItemDetails);
            object.id = e.currentTarget.dataset.id;
            object.img = e.currentTarget.dataset.img;
            object.price = e.currentTarget.dataset.price;
            object.title = e.currentTarget.dataset.title;
            onItemClick(object);
          }}
        >
          {" "}
          <div
            className="bg-light "
            style={{
              width: "121.79px",
              height: "210.71px",
            }}
          >
            <img src={link} alt="" className=" card-img-top" />
          </div>{" "}
        </Link>
        <div
          className="d-flex justify-content-between card-body text-dark m-0 pl-2 pr-2 bg-dark"
          style={{ height: "89.77px" }}
        >
          <Link
            to={`/products/detalization/${id}`}
            className="btn stretched-link"
            data-title={title}
            data-img={link}
            data-price={price}
            data-id={id}
            onClick={(e) => {
              let object = Object.assign(currentItemDetails);
              object.id = e.currentTarget.dataset.id;
              object.img = e.currentTarget.dataset.img;
              object.price = e.currentTarget.dataset.price;
              object.title = e.currentTarget.dataset.title;
              onItemClick(object);
            }}
          >
            <div>
              <h4
                className="card-title text-left  text-success"
                style={{ fontSize: "1rem" }}
              >
                {title}
              </h4>
            </div>
          </Link>

          <div>
            <h6 className="card-text text-right text-warning">
              {" "}
              {price}
            </h6>
          </div>
          <button
            data-id={id}
            onClick={(e) => {
              onAdd(e.currentTarget.dataset.id);
            }}
          >
            Add to card
          </button>
        </div>{" "}
      </div>
    </div>
  );
};

export default CardElectronics;

CardElectronics.propTypes={
  id: PropTypes.string,
  title:PropTypes.string,
  link:PropTypes.string,
  price:PropTypes.number,
  currentItemDetails:PropTypes.object,
  onItemClick:PropTypes.func,
  onAdd:PropTypes.func,
}
