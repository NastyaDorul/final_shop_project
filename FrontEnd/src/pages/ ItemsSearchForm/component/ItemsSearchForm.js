import React from "react";
import PropTypes from "prop-types";
import Preloader from "../../../components/Preloader/index";

const ItemsSearchForm = ({
  searchIsReady,
  searchResult,
  searchText,
  parentCategory,
}) => {
  return (
    <div className="m-5 p-5 container-fluid d-flex flex-wrap justify-content-center bg-light">
      {!searchIsReady ? (
        <Preloader />
      ) : (
        <div>
          {!searchResult ? (
            <p>Noting was found for request: {`"${searchText}"`} </p>
          ) : (
            <div>
              {parentCategory === "electronics" && (
                <>
                  <p>Results of your request: {`"${searchText}"`} </p>
                  <p>{!searchResult.title}</p>
                  <img src={searchResult.image} alt="" />
                  <p>Description: {searchResult.description}</p>
                  <p>Price: {searchResult.price}</p>
                  <p>Brand: {searchResult.brand}</p>
                  <p>Genre: {searchResult.genre}</p>
                  <p>Gaming System: {searchResult.gamingSystem}</p>
                  <p>Game Rating: {searchResult.gameRating}</p>
                  <p>Availability: {searchResult.availability}</p>
                </>
              )}
              {parentCategory === "womans" && (
                <>
                  <p>Results of your request: {`"${searchText}"`} </p>
                  <p>{!searchResult.name}</p>

                  <img src={searchResult.images[0]} alt="sneakers" />
                  <p>Code: {searchResult.code}</p>
                  <p>Price: {searchResult.details.price}</p>
                  <p>Brand: {searchResult.brand}</p>
                  <p>
                    Available Sizes:{" "}
                    <span>
                      {searchResult.size[0]}
                      {", "}
                    </span>
                    <span>
                      {searchResult.size[1]}
                      {", "}
                    </span>
                    <span>{searchResult.size[2]}</span>
                  </p>

                  <p>Material: {searchResult.details.material}</p>
                  <p>Season: {searchResult.details.season}</p>
                  <p>
                    Producing Country: {searchResult.details.producingcountry}
                  </p>
                </>
              )}
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default ItemsSearchForm;

ItemsSearchForm.propTypes={
  searchIsReady:PropTypes.bool.isRequired,
  searchResult:PropTypes.object,
  searchText:PropTypes.string,
  parentCategory:PropTypes.string,
}
