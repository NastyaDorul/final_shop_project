import React from "react";
import ItemSearchForm from "../../ ItemsSearchForm/component";

import {useSelector} from "react-redux";

function ItemSearchFormContainer() {
  
      const searchIsReady=useSelector((state)=>state.search.searchIsReady);
      const searchResult= useSelector((state)=>state.search.searchResult);
      const searchText=useSelector((state)=>state.search.searchText);
      const parentCategory=useSelector((state)=>state.result.parentCategory);

    return (
      <ItemSearchForm
        searchIsReady={searchIsReady}
        searchResult={searchResult}
        searchText={searchText}
        parentCategory={parentCategory}
      />
    );

}

export default ItemSearchFormContainer;