import React from "react";
import { useSelector,useDispatch} from "react-redux";
import Preloader from "../../../components/Preloader/index";
import CardElectronics from "../../CardPageGames/index";
import SidebarComponent from "../../Sidebar/index";
import WellcomePageProfile from "../../WellcomePageProfile/container/ WellcomePageProfile";
import CardWomans from "../../CardPageWomans/index";


import {
    setCurrentPageAc,
    setPageLimitAc,
    getProductsThunkCreator,
    setCurrentTypeOfSortAc,
    setItemIdTitlePriceImgAc,
    addToCartAc,
  } from "../../../store/actions/Results";

const Results = () => {

const totalProductCount=useSelector(state=>state.result.totalProductCount); 
const limit=useSelector(state=>state.result.limit);
const currentPage=useSelector(state=>state.result.currentPage);
const parentCategory=useSelector(state=>state.result.parentCategory);//з компонента WellcomePage
const selectedTypeOfSort=useSelector(state=>state.result.selectedTypeOfSort);
const selectedColorFilter=useSelector(state=>state.result.selectedColorFilter);
const selectedSizeFilter=useSelector(state=>state.result.selectedSizeFilter);
//----------------other-props--for--sub-components----------------------------------------
const result=useSelector((state)=>state.result.result);
const isReadyAfterRefresh=useSelector((state)=>state.result.isReadyAfterRefresh);
//----------------other-props--for--sub-components---can't--find--------------------------
const isReady=useSelector((state)=>state.result.isReady);
const pageSize=useSelector((state)=>state.result.pageSize);
const currentPageResult=useSelector((state)=>state.result.currentPageResult); 
//----------------other-props--for--CardPageGames-----------------------------------------
const currentItemDetails=useSelector((state)=>state.result.currentItemDetails);


const dispatch=useDispatch();
const setCurrentPage=(currentPage) => dispatch(setCurrentPageAc(currentPage));
const setPageLimit=(limit) => dispatch(setPageLimitAc(limit));
const setCurrentTypeOfSort=(selectedTypeOfSort)=>dispatch(setCurrentTypeOfSortAc(selectedTypeOfSort));

const setProductsThunk=(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter)=>{
    dispatch(getProductsThunkCreator(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter));
};
const setItemIdTitlePriceImg=(currentItemDetails) => {
  dispatch(setItemIdTitlePriceImgAc(currentItemDetails))
};
const addToCart=(id)=>{
dispatch(addToCartAc(id));
}

const onItemClick = (currentItemDetails) => {
  setItemIdTitlePriceImg(currentItemDetails);
  console.log(currentItemDetails);
};

const onAdd = (id) => {
  addToCart(id);
  console.log("Add this to card!", id);
};

const onPageChanged = (pageNumber) => {
    setCurrentPage(pageNumber);
    setPageLimit(limit);
    console.log(pageNumber,limit);
    setProductsThunk(parentCategory,pageNumber,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter);
  };

const onSortByChanged = (selectedTypeOfSort) => {
    setCurrentTypeOfSort(selectedTypeOfSort);
    console.log(selectedTypeOfSort,limit, currentPage);
    setProductsThunk(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter);
  };

  
const onLimitChanged = (limit) => {
    setPageLimit(limit);
    console.log(limit,currentPage,selectedTypeOfSort);
    setProductsThunk(parentCategory,currentPage,limit,selectedTypeOfSort,selectedColorFilter,selectedSizeFilter);
  };

  let pagesCount = Math.ceil(totalProductCount / limit);
  let pages = [];
  //console.log(props);

  for (let i = 1; i <= pagesCount; i++) {
    pages.push(i);
  }

  return (
    <div>
      <div className="d-flex justify-content-start ">
        <WellcomePageProfile />
      </div>

      <div className="mb-5 pl-5 pr-5 d-flex  justify-content-between bg-light">
        <div>
          {pages.map((p, i) => {
            return (
              <>
                <span
                  style={{
                    margin: "10px",
                    cursor: "pointer",
                    fontWeight: "bold",
                  }}
                  key={i}
                  className={currentPage === p && "selected-Page"}
                  onClick={() => {
                    onPageChanged(p);
                  }}
                >
                  {p}
                </span>
              </>
            );
          })}
          <span>
            {" "}
            Showing {limit} of {totalProductCount} Results{" "}
          </span>
        </div>
        <div className="d-flex">
          <form>
            <select
              onChange={(e) => {
                onSortByChanged(e.target.value);
              }}
            >
              <option value="">Select sort displaying...</option>
              <option value="Price Hight To Low">Price Hight To Low</option>
              <option value="Price Low To Hight">Price Low To Hight</option>
              <option value="Best Matches">Best Matches</option>
              <option value="Most Popular">Most Popular</option>
              <option value="Brand">Brand</option>
              <option value="Top Sellers">Top Sellers</option>
              <option value="Product Name A-Z">Product Name A-Z</option>
              <option value="Product Name Z-A">Product Name Z-A</option>
            </select>
          </form>

          <form>
            <select
              onChange={(e, currentPage) => {
                onLimitChanged(e.target.value, currentPage);
              }}
            >
              <option value="">Select page</option>
              {/* поставити перевірку на кількість товарів всього. Якщо менше, то не відображати відповідну опцію  */}
              <option value="6">6</option>
              <option value="12">12</option>
              <option value="24">24</option>
              <option value="36">36</option>
              <option value="48">48</option>
              <option value="60">60</option>
            </select>
          </form>
        </div>
      </div>

      <div className=" d-flex ">
        <div className="p-4 d-flex flex-column border">
          <SidebarComponent/>
        </div>
        <div>
          {" "}
          <div className=" d-flex flex-wrap p-3 border justify-content-around">
            {!isReadyAfterRefresh ? (
              <Preloader />
            ) : (
              result.map((item, i) => (
                <div key={i}>
                  <div className="col-md-1">
                    
                    {parentCategory === "electronics" && (
                      <CardElectronics
                        className="flex-fill"
                        title={item.title}
                        link={item.image ? item.image : item.defaultImage}
                        price={item.price}
                        id={item._id}
                        onItemClick={onItemClick}
                        currentItemDetails={currentItemDetails}
                        onAdd={onAdd}
                      />
                    )}
                    {parentCategory === "womans" && (
                      <CardWomans
                        className="flex-fill"
                        title={item.brand}
                        name={item.name}
                        link={item.images ? item.images[0] : item.defaultImage}
                        price={item.details.price}
                        id={item._id}
                        onItemClick={onItemClick}
                        currentItemDetails={currentItemDetails}
                        onAdd={onAdd}
                      />
                    )}
                  </div>
                </div>
              ))
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Results;
