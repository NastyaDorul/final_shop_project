import React from "react";
import PropTypes from "prop-types";

import "./shopping-cart.scss";

const ShoppingCartTable = ({
  items,
  total,
  onIncrease,
  onDecrease,
  onDelete,
}) => {
  console.log(items);
  const renderRow = (item, idx) => {
    const { id, title, count, total } = item;
    const { model, code, brand } = item;

    return (
      <tr key={id}>
        <td>{idx + 1}</td>
        <td>
          {title}
          {"  model "}
          {model}
          {"  code  "}
          {code}
          {"  "}
          {brand}
        </td>
        <td>{count}</td>
        <td>${total.toFixed(2)}</td>
        {/* not finished */}
        <td>
          <button
            onClick={() => onDelete(id)}
            className="btn btn-outline-danger btn-sm float-right"
          >
            <i className="fa fa-trash" />
          </button>
          <button
            onClick={() => onIncrease(id)}
            className="btn btn-outline-success btn-sm float-right"
          >
            <i className="fa fa-plus-circle" />
          </button>
          <button
            onClick={() => onDecrease(id)}
            className="btn btn-outline-warning btn-sm float-right"
          >
            <i className="fa fa-minus-circle" />
          </button>
        </td>
      </tr>
    );
  };

  return (
    <div className="shopping-cart-table">
      <h2>Your Order</h2>
      <table className="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Item</th>
            <th>Count</th>
            <th>Price</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>{items.map(renderRow)}</tbody>
      </table>

      <div className="total">Total: ${total}</div>
    </div>
  );
};
export default ShoppingCartTable;

ShoppingCartTable.propTypes={
  items:PropTypes.array,
  total:PropTypes.number,
  onIncrease:PropTypes.func.isRequired,
  onDecrease:PropTypes.func.isRequired,
  onDelete:PropTypes.func.isRequired,
}