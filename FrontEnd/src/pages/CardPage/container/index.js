import React from "react";
import {useSelector,useDispatch } from "react-redux";


import {
  addToCartAc,
  removeFromCartAc,
  removeAllFromCart,
} from "../../../store/actions/Results";
import ShoppingCartTable from "../component/index";

const ShoppingCart=()=> {

const items=useSelector((state)=>state.result.cartItems);    
//const total=useSelector((state)=>state.result.shoppingCartTotal.toFixed(2));
//const count=useSelector((state)=>state.result.shoppingCartCount);
const shoppingCartTotal=useSelector((state)=>state.result.cartItems
.reduce((total, item) => total + item.total, 0)
.toFixed(2));

const dispatch=useDispatch();
const onIncrease=(id) => dispatch(addToCartAc(id));
const onDecrease=(id)=>this.dispatch(removeFromCartAc(id));
const onDelete=(id)=>dispatch(removeAllFromCart(id));

    return (
      <ShoppingCartTable
        items={items}
        total={shoppingCartTotal}
        onIncrease={onIncrease}
        onDecrease={onDecrease}
        onDelete={onDelete}
      />
    );
  }

export default ShoppingCart;
