import React from "react";
import {useSelector} from "react-redux";
import WellcomePageItem from "../component/WellcomePageItem";
import "../style.scss";


function WellcomePageItemContainer() {
  const parentCategory=useSelector((state=>state.result.parentCategory));
  const subCategory=useSelector((state=>state.result.subCategory));       

    return (
      <div className="m-5 p-5 container d-flex justify-content-start">
        <WellcomePageItem
          subCategory={subCategory}
          parentCategory={parentCategory}
        />
      </div>
    );
  
}

export default WellcomePageItemContainer;