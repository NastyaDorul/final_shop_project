import React from "react";
import {useSelector} from "react-redux";
import WellcomePageProfile from "../component/WellcomePageProfile";
import "../style.scss";

function WellcomePageProfileContainer(){
 const parentCategory=useSelector((state)=>state.result.parentCategory);
 const subCategory=useSelector((state)=> state.result.subCategory);

    return (
      <div className="m-5 p-5 container d-flex justify-content-start">
        <WellcomePageProfile
          subCategory={subCategory}
          parentCategory={parentCategory}
        />
      </div>
    );
}

export default WellcomePageProfileContainer;
