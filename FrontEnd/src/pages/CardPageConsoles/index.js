import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const Card = ({link,id,setCurrentConsoleItemId,currentConsoleItemId,
title,price,currentConsoleItem}) => {
  
  return (
    <div
      className="card-deck border shadow p-4 mb-4 bg-white"
      style={{ width: "18rem" }}
    >
      <div>
        <img src={link} alt="" className="card-img-top" />
      </div>
      <div className="card-body card-text-center text-dark ">
        <Link
          to={`/products/gaming-Game-consoles/search-By/${id}`}
          className="btn stretched-link"
          onClick={() => {
            setCurrentConsoleItemId(id, currentConsoleItemId);
          }}
        >
          <h4 className="card-title text-success" style={{ fontSize: "1rem" }}>
            {title}
          </h4>
        </Link>
        <h6 className="card-text text-secondary"> {price}</h6>
      </div>
    </div>
  );
};

export default Card;

Card.propTypes={
  link:PropTypes.string,
  id:PropTypes.string,
  setCurrentConsoleItemId:PropTypes.func.isRequired,
  currentConsoleItemId:PropTypes.string,
  title:PropTypes.string,
  price:PropTypes.number,
}
