//React
import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
//Redux
import { Provider } from "react-redux";
import store from "./store/";
//Pages
import HomePage from "./pages/Home";

import ItemsConsoles from "./pages/ItemsConsoles/containers/ItemsConsoles";
import ResultContainer from "./pages/Results/container/Results";

import NavbarContainer from "../src/modules/Navbar/container/index";


import ItemsSearchForm from "../src/pages/ ItemsSearchForm/container/ ItemSearchForm";
import Container from "../src/components/Container/index";
import WellcomePageProfile from "./pages/WellcomePageProfile/container/ WellcomePageProfile";
import WellcomePageItem from "./pages/WellcomePageProfile/container/WellcomePageItem";
import MenuComponent from "./modules/Menu/container/index";
import { useRoutes } from "./pages/routes";
import "./_reset.scss";


const App = (props) => {
  const routes = useRoutes(false);
  console.log(props);
  return (
    <Provider store={store}>
      <Router>
        <Container>
          <MenuComponent />
        </Container>
        <Container>
          <NavbarContainer />
        </Container>
        {routes}
      

        {/*there are should be a route for a home page*/}
      </Router>
    </Provider>
  );
};
export default App;
