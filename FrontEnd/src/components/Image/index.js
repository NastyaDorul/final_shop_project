import React from "react";
import { BrowserRouter as Link } from "react-router-dom";
import PropTypes from "prop-types";

const Image = ({ src, alt, link }) => {
  const Img = link ? (
    <Link to={link}>
      <div>
        <img src={src} alt={alt} />
      </div>
    </Link>
  ) : (
    <div>
      <img src={src} alt={alt} />
    </div>
  );
  return { Img };
};
export default Image;


Image.propTypes = {
  src : PropTypes.string.isRequired,
  alt : PropTypes.string.isRequired,
  link : PropTypes.string.isRequired
};
