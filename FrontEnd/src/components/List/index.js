import React from "react";
import ListItemComponent from "./ListItem";
import PropTypes from "prop-types";

const ListComponent = (props,{ listHeader}) => {
  const {categoryName,onSubCategoryChanged,onParentCategoryChanged,listLink,listItems}=props;

  let listI = listItems.map((elem, index) => {  
    return (
      <div key={index}>
        <ListItemComponent
          array={elem}
          linkItems={listLink}
          onParentCategoryChanged={onParentCategoryChanged}
          onSubCategoryChanged={onSubCategoryChanged}
          categoryName={categoryName}
        />
      </div>
    ); //сделать переборку массива
  });

  return (
    <ul>
      {listHeader && <p>{listHeader}</p>}
      {listI}
    </ul>
  );
};
export default ListComponent;

ListComponent.propTypes={
  listItems:PropTypes.array.isRequired,
 
}
ListItemComponent.propTypes={
  listLink:PropTypes.string,
  categoryName:PropTypes.string.isRequired,
  onParentCategoryChanged:PropTypes.func.isRequired,
  onSubCategoryChanged:PropTypes.func.isRequired,
}