import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import "./style.scss";

const ListItemComponent = ({categoryName,array,onSubCategoryChanged,onParentCategoryChanged}) => {
  console.log(categoryName.toLowerCase());
  return (
    <Link
      to={`/category?parentCategory=${categoryName
        .toLowerCase()
        .split(" ")
        .join("")}&subCategory=${array}`}
    >
      <div
        className="dropdown-item"
        data-array={array}
        data-parent={categoryName}
        onClick={(e, subCategory, parentCategory) => {
          onSubCategoryChanged(e.target.dataset.array, subCategory);
          onParentCategoryChanged(
            e.target.dataset.parent,
            parentCategory
          );
        }}
      >
        {array}
      </div>
    </Link>
  );
};
export default ListItemComponent;

ListItemComponent.propTypes = {
  categoryName: PropTypes.string.isRequired,
  array : PropTypes.array.isRequired,
  onSubCategoryChanged : PropTypes.func.isRequired,
  onParentCategoryChanged:PropTypes.func.isRequired,
};