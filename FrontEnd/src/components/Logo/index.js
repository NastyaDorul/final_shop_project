import React from "react";
import { Link } from "react-router-dom";

import logo from "../../assets/images/logo.png";

const Logo = () => {
  return (
    <div>
      <Link to="/" className="navbar-brand">
        <img src={logo} alt="logo" />
      </Link>
    </div>
  );
};
export default Logo;

