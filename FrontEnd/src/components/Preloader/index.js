import React from "react";
import preloader from "../../assets/images/830.gif";

const Preloader = () => {
  return <img src={preloader} alt="preloader" />;
};
export default Preloader;
