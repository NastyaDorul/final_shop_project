import React from "react";
import { Link } from "react-router-dom";
import { useSelector} from "react-redux";
import Logo from "../../../components/Logo/index";

import SearchComponent from "../../../modules/Search/index"; //not my search
import {
  faMapMarkerAlt,
  faUserAlt,
  faShoppingCart,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const MenuComponent = () => {
 
  const shoppingCartTotal=useSelector((state)=>state.result.cartItems
      .reduce((total, item) => total + item.total, 0)
      .toFixed(2));
  const shoppingCartCount=useSelector((state)=>state.result.cartItems.reduce(
      (count, item) => count + item.count,
      0
    ));

  return (
    <>
      <Logo />
      <SearchComponent />
      <Link to="/geo">
        <FontAwesomeIcon
          icon={faMapMarkerAlt}
          size="2x"
          style={{ color: "grey" }}
        />
      </Link>

      <Link to="/user">
        <FontAwesomeIcon icon={faUserAlt} size="2x" style={{ color: "grey" }} />
      </Link>
      <Link to="/products/cart">
        <FontAwesomeIcon
          icon={faShoppingCart}
          size="2x"
          style={{ color: "grey" }}
        />
      </Link>
      <p>
        <strong>{shoppingCartCount} items </strong> ({shoppingCartTotal}
        {"$"})
      </p>
    </>
  );
};

export default MenuComponent;
