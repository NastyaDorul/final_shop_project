import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

import Button from "../../components/Button";
import Input from "../../components/Input";
import "./style.scss";


const SearchComponent=()=>{
    const [searchText, setSearch] = useState("");

    const HandleChange=(e)=>{
        setSearch(e.target.value);
    }
   

    return (
        <div className="form-inline my-2 my-lg-0">
          <Input
            className="ownstyle-input mr-sm-2"
            onChange={HandleChange}
            placeHolder="Enter Keyword or Item No."
            type="search"
            aria-label="Search"
          />
  
          <Button
            className="ownstyle-button my-2 my-sm-0"
            type="submit"
            onClick={() => console.log(searchText)}
            text={<FontAwesomeIcon icon={faSearch} style={{ color: "grey" }} />}
          />
        </div>
      );


}

export default SearchComponent;
