import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import ListComponent from "../../../components/List";
import HoverComponent from "../../../components/Hover";



const NavbarItemComponent = ({className,categoryItems,onParentCategoryChanged,
onSubCategoryChanged,onResultRefresh,categoryName}) => {
  return (
    <li className={className}>
      <HoverComponent
        onHover={
          <ListComponent
            listItems={categoryItems}
            onParentCategoryChanged={onParentCategoryChanged}
            onSubCategoryChanged={onSubCategoryChanged}
            onResultRefresh={onResultRefresh}
            categoryName={categoryName}
          />
        }
      >
        <Link
          to={`/parentCategory?${categoryName.toLowerCase()}`}
          onClick={(e, parentCategory, subCategory) => {
            onParentCategoryChanged(
              e.target.innerText.toLowerCase(),
              parentCategory
            );
            onResultRefresh(
              e.target.innerText.toLowerCase(),
              parentCategory
            );
            onSubCategoryChanged("", subCategory);
          }}
        >
          {categoryName}
        </Link>
        {/*добавила Link */}
        <div></div>
      </HoverComponent>
    </li>
  );
};
export default NavbarItemComponent;

ListComponent.propTypes={
  className:PropTypes.string,
  categoryItems:PropTypes.string,
  onParentCategoryChanged:PropTypes.func.isRequired,
  onSubCategoryChanged:PropTypes.func.isRequired,
  onResultRefresh:PropTypes.func.isRequired,
  categoryName:PropTypes.string
}
