import React from "react";
import {useSelector,useDispatch} from "react-redux";
import NavbarItemComponent from "../NavbarItem";
import {setSubCategoryAc,
  setSortingPriceAc,
  getNewCategoriesOnParentCategoryChangedThunkCreator,
  getSettingsOnResultRefreshThunkCreator,} from "../../../store/actions/Results"; 

import "../style.scss";

const NavbarComponent = () => {

  const NavbarArray = [
    {
      name: "new arrivals",
      arr: ["womans", "mans", "electronics"],
    },
    {
      name: "womans",
      arr: [
        "clothing",
        "outfits",
        "tops",
        "dresses",
        "bottoms",
        "jackets & coats",
        "feeling red",
        "jewelry",
        "earings",
        "bracelets",
        "necklaces",
        "accessories",
        "scarves",
        "shoes",
      ],
    },
    {
      name: "mens",
      arr: [
        "clothing",
        "suits",
        "jackets & coats",
        "dress shirts",
        "shorts",
        "pants",
        "accessories",
        "ties",
        "gloves",
        "luggage",
      ],
    },
    {
      name: "electronics",
      arr: [
        "televisions",
        "digital cameras",
        "ipods & mp3 players",
        "gps navigations",
        "gaming",
      ],
    },
    { name: "top sellers", arr: ["top sellers"] },
  ];

 
  const limit=useSelector((state)=>state.result.limit); 
  const currentPage=useSelector((state)=>state.result.currentPage);
  const selectedTypeOfSort=useSelector((state)=>state.result.selectedTypeOfSort);
  
  
  const dispatch=useDispatch();
  const setSubCategory=(subCategory)=>dispatch(setSubCategoryAc(subCategory));
  const setNewCategoriesOnParentCategoryChangedThunk=(parentCategory)=>dispatch(getNewCategoriesOnParentCategoryChangedThunkCreator(parentCategory));
  const setSettingsOnResultRefreshThunk=(parentCategory,currentPage,limit,selectedTypeOfSort)=>dispatch(getSettingsOnResultRefreshThunkCreator(parentCategory,currentPage,limit,selectedTypeOfSort));
  
  const setSortingPrice=(sortingPriceItems) =>dispatch(setSortingPriceAc(sortingPriceItems));
  //---one-price-ranges-for-all-categories----------------------------------------------------
  
  const onSubCategoryChanged = (subCategory) => {
    setSubCategory(subCategory);
    console.log(subCategory);
  }; 
  
  const onParentCategoryChanged = (parentCategory) => {
    console.log(parentCategory);
    setNewCategoriesOnParentCategoryChangedThunk(parentCategory);
  };


  const onResultRefresh = (parentCategory) => {
    console.log(
      "We have to Refresh result for new parentCategory",
      parentCategory,
      currentPage
    );
    setSettingsOnResultRefreshThunk(parentCategory,currentPage,limit,selectedTypeOfSort);
  };



  let NavbarItems = NavbarArray.map((elem, index) => {
    return (
      <NavbarItemComponent
        key={index}
        categoryName={elem.name.toUpperCase()}
        categoryItems={elem.arr}
        className="navbar-item"
        onParentCategoryChanged={onParentCategoryChanged}
        onSubCategoryChanged={onSubCategoryChanged}
        onResultRefresh={onResultRefresh}
      />
    );
  });
  return <nav className="navbar">{NavbarItems}</nav>;
};
export default NavbarComponent;
